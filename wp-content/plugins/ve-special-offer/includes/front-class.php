<?php
class Ve_Special_Offer_Front {

    public static function enqueue_scripts (){

        wp_enqueue_script('jquery');

        wp_enqueue_script( 've-special-offer-front-script', VE_SPECIAL_OFFER_PLUGIN_URL . 'assets/js/front.js', array('jquery'), VE_SPECIAL_OFFER_PLUGIN_VERSION );
        wp_enqueue_style('ve-special-offer-front-style', VE_SPECIAL_OFFER_PLUGIN_URL . 'assets/css/front.css', array(), VE_SPECIAL_OFFER_PLUGIN_VERSION);

    }
}
<?php
class Ve_Special_Offer_Shortcode {

    public static function add_shortcode($atts) {

        $params = shortcode_atts( array(
            'offer_id' => '',
        ), $atts );

        if ($params['offer_id']){

            $post_helper = new Ve_Post_Helper();


            $special_offers_ids = explode(',', $params['offer_id']);

            $args = array(
                'post_type' => 'special_offer',
                'post__in' => $special_offers_ids,
                'numberposts' => 0,
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'posts_per_page' => -1,
            );

            $special_offers = get_posts($args);

            if ($special_offers && is_array($special_offers)){

                foreach ($special_offers as $offer_key => $special_offer) {
                    $special_offers[$offer_key]->custom_fields = $post_helper->get_posts_meta($special_offer);
                }

                ob_start();
                require_once( VE_SPECIAL_OFFER_PLUGIN_DIR . 'templates/special-offer-items.php');
                $html = ob_get_clean();
                return $html;
            }
        }

    }
}
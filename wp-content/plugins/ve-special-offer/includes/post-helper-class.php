<?php

class Ve_Post_Helper {

    public function get_posts_meta($post) {

        $custom_fields_array = false;

        if ( function_exists('get_fields') ) {

            $custom_fields = get_fields($post->ID);

            if ( ! empty( $custom_fields ) ) {
                foreach ($custom_fields as $custom_field_key => $custom_field) {
                    if ( is_serialized($custom_field[0]) ) {
                        $custom_fields_array[$custom_field_key] = unserialize($custom_field[0]);
                    } else {
                        $custom_fields_array[$custom_field_key] = $custom_field;
                    }
                }
            }

        } else {

            $custom_fields = get_post_meta($post->ID);

            if ( ! empty( $custom_fields ) ) {
                foreach ($custom_fields as $custom_field_key => $custom_field) {
                    if ( is_serialized($custom_field[0]) ) {
                        $custom_fields_array[$custom_field_key] = unserialize($custom_field[0]);
                    } else {
                        $custom_fields_array[$custom_field_key] = $custom_field[0];
                    }
                }
            }

        }

        return $custom_fields_array;
    }

    public function getTimestampFromString($string, $format)
    {
        $dateInfo = date_parse_from_format($format, $string);
        if ($dateInfo['error_count'] > 0){
            return false;
        }

        $timestamp = mktime($dateInfo['hour'], $dateInfo['minute'], $dateInfo['second'], $dateInfo['month'], $dateInfo['day'], $dateInfo['year']);

        return $timestamp;
    }
}
<?php
class Ve_Special_Offer_Admin {

    public static function register_settings (){

        register_setting('ve_special_offer_settings_group', 've_special_offer_settings', array( 'Ve_Special_Offer_Admin', 'sanitize_options') );

    }

    public static function sanitize_options( $options ){
        return $options;
    }

    public static function set_settings_link (){

        add_options_page('Special Offer options', 'Special Offer options', 'manage_options', 've-special-offer-options', array( 'Ve_Special_Offer_Admin' , 'settings_page'));

    }

    public static function settings_page() {

        $options = Ve_Special_Offer::get_options();
        require_once( VE_SPECIAL_OFFER_PLUGIN_DIR . 'includes/settings-form.php' );

    }

    public static function enqueue_scripts (){

        wp_enqueue_script('jquery');

        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'wp-color-picker' );

        wp_enqueue_script( 've-special-offer-admin-script', VE_SPECIAL_OFFER_PLUGIN_URL . 'assets/js/admin.js', array('jquery', 'wp-color-picker'), VE_SPECIAL_OFFER_PLUGIN_VERSION );
        wp_enqueue_style('ve-special-offer-admin-style', VE_SPECIAL_OFFER_PLUGIN_URL . 'assets/css/admin.css', array(), VE_SPECIAL_OFFER_PLUGIN_VERSION);

    }

    public static function add_action_links( $links ) {

        $url = get_admin_url(null, 'options-general.php?page=ve-special-offer-options');

        $links[] = '<a href="'. $url .'">Settings</a>';
        return $links;
    }

    public static function save_default_options() {

        $options = get_option( 've_special_offer_settings' );

        if( empty($options) ) {
            $options = Ve_Special_Offer::get_options();
            update_option( 've_special_offer_settings', $options );
        }

        return;



    }

}
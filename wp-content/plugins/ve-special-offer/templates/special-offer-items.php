<div class="special_offers_wrapper">
    <?php foreach ($special_offers as $special_offer) : ?>
        <div class="row_line special_offer">
            <div class="col_width_6">
                <?php if ($special_offer->custom_fields['image']) : ?>
                    <img src="<?php esc_attr_e($special_offer->custom_fields['image']); ?>" alt="<?php _e($special_offer->post_title); ?>">
                <?php else : ?>
                    <img src="https://imagesvc.timeincapp.com/v3/mm/image?url=https%3A%2F%2Fcdn-image.travelandleisure.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2F1600x1000%2Fpublic%2Fhotel-interior-room0416.jpg%3Fitok%3D5gENxAK1&w=700&q=85" alt="<?php _e($special_offer->post_title); ?>">
                <?php endif; ?>
            </div>
            <div class="col_width_6">
                <div class="row_line row_line_center special_offer_hotel_data_wrapper">
                    <div class="col_width_12">
                        <b><?php esc_html_e($special_offer->post_title); ?></b>
                    </div>
                    <div class="col_width_12">
                        <i><?php esc_html_e($special_offer->custom_fields['room_name']); ?></i>
                    </div>
                    <div class="col_width_12">
                        $ <span class="offer_cost"><?php esc_html_e($special_offer->custom_fields['cost']); ?></span> USD/NIGHT | <?php esc_html_e($special_offer->custom_fields['rate_name']); ?> STAR RATE
                    </div>
                    <div class="col_width_12 special_offer_date_wrapper">
                        <div class="row_line">
                            <div class="col_width_6">
                                <?php $arrival_date = $post_helper->getTimestampFromString($special_offer->custom_fields['arrival_date'], 'Ymd'); ?>
                                <?php $departure_date = $post_helper->getTimestampFromString($special_offer->custom_fields['departure_date'], 'Ymd'); ?>
                                <div class="special_offer_date_selector_wrapper" data-date-start="<?php esc_attr_e($arrival_date); ?>">
                                    <span class="special_offer_date_type"><?php _e("Arrival", "ve_special_offer"); ?></span>
                                    <div class="special_offer_calendar_wrapper arrival">
                                        <p class="special_offer_month" data-month="<?php _e(date('n', $arrival_date)); ?>"><i><?php _e(date('F', $arrival_date)); ?></i></p>
                                        <p class="special_offer_day" data-day="<?php _e(date('j', $arrival_date)); ?>"><i><?php _e(date('j', $arrival_date)); ?></i></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col_width_6">
                                <div class="special_offer_date_selector_wrapper" data-date-end="<?php esc_attr_e($departure_date); ?>">
                                    <span class="special_offer_date_type"><?php _e("Departure", "ve_special_offer"); ?></span>
                                    <div class="special_offer_calendar_wrapper departure">
                                        <p class="special_offer_month" data-month="<?php _e(date('n', $departure_date)); ?>"><i><?php _e(date('F', $departure_date)); ?></i></p>
                                        <p class="special_offer_day" data-day="<?php _e(date('j', $departure_date)); ?>"><i><?php _e(date('j', $departure_date)); ?></i></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row_line">
                <div class="col_width_6 special_offer_total_price_wrapper">
                    <span class="special_offer_total_price_label"><b><?php _e("Total price", "ve_special_offer"); ?></b></span>
                </div>
                <div class="col_width_6 special_offer_total_price_wrapper row_line_right">
                    <b>$ <span class="special_offer_total_price_value"><?php echo (($departure_date - $arrival_date) / 86400 + 1) * $special_offer->custom_fields['cost']; ?></span></b>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="special_offer_date_selector_popup col_width_9" style="display: none;">
        <span class="close_popup">x</span>
        <div class="special_offer_popup_error"><?php _e("Arrival date can't be more than departure date", "ve_special_offer"); ?></div>
        <form>
            <div class="col_width_8">
                <label class="col_width_3"><?php _e('Day', 've_special_offer'); ?></label>
                <select class="col_width_9 special_offer_date_selector_day" name="day"></select>
            </div>
            <div class="col_width_8">
                <label class="col_width_3"><?php _e('Month', 've_special_offer'); ?></label>
                <select class="col_width_9 special_offer_date_selector_month" name="month">
                    <option value="1"><?php _e('January', 've_special_offer'); ?></option>
                    <option value="2"><?php _e('February', 've_special_offer'); ?></option>
                    <option value="3"><?php _e('March', 've_special_offer'); ?></option>
                    <option value="4"><?php _e('April', 've_special_offer'); ?></option>
                    <option value="5"><?php _e('May', 've_special_offer'); ?></option>
                    <option value="6"><?php _e('June', 've_special_offer'); ?></option>
                    <option value="7"><?php _e('July', 've_special_offer'); ?></option>
                    <option value="8"><?php _e('August', 've_special_offer'); ?></option>
                    <option value="9"><?php _e('September', 've_special_offer'); ?></option>
                    <option value="10"><?php _e('October', 've_special_offer'); ?></option>
                    <option value="11"><?php _e('November', 've_special_offer'); ?></option>
                    <option value="12"><?php _e('December', 've_special_offer'); ?></option>
                </select>
            </div>
            <div class="col_width_8">
                <input type="submit">
            </div>
        </form>
    </div>
</div>

<script>
    var monthNames = {
        month1: '<?php _e('January', 've_special_offer'); ?>',
        month2: '<?php _e('February', 've_special_offer'); ?>',
        month3: '<?php _e('March', 've_special_offer'); ?>',
        month4: '<?php _e('April', 've_special_offer'); ?>',
        month5: '<?php _e('May', 've_special_offer'); ?>',
        month6: '<?php _e('June', 've_special_offer'); ?>',
        month7: '<?php _e('July', 've_special_offer'); ?>',
        month8: '<?php _e('August', 've_special_offer'); ?>',
        month9: '<?php _e('September', 've_special_offer'); ?>',
        month10: '<?php _e('October', 've_special_offer'); ?>',
        month11: '<?php _e('November', 've_special_offer'); ?>',
        month12: '<?php _e('December', 've_special_offer'); ?>',
    };
</script>
<?php
/*
Plugin Name: Special Offers
Plugin URI: http://localhost
Description: This plugin allow to handle special offers in hotels
Version: 0.1.0
Author: Victor Efimov
Author URI: http://localhost
License: GPLv2
*/

// If this file is called directly, exit.
if ( !defined( 'ABSPATH' ) ) exit;

if (!class_exists('Ve_Special_Offer')){

    class Ve_Special_Offer {
        private static $instance = null;

        public $default_options = array();
        public static $options = null;

        public static function instance (){

            if( self::$instance == null ){

                self::$instance = new Ve_Special_Offer();
                self::$instance->constants();
                self::$instance->includes();
                self::$instance->load_localisation_files();

            }

            return self::$instance;
        }

        private function __construct() {
            $this->default_options = array(
                'special_offer_post_type' => 've_special_offer'
            );

            add_action( 'admin_init', array('Ve_Special_Offer_Admin', 'register_settings'));
            add_action( 'admin_menu', array( 'Ve_Special_Offer_Admin', 'set_settings_link' ));
            add_action( 'admin_enqueue_scripts', array( 'Ve_Special_Offer_Admin', 'enqueue_scripts' ));

            add_action( 'wp_enqueue_scripts', array( 'Ve_Special_Offer_Front', 'enqueue_scripts' ), 20);

            add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), array( 'Ve_Special_Offer_Admin', 'add_action_links') );

            register_activation_hook( __FILE__, array( 'Ve_Special_Offer_Admin', 'save_default_options') );
            add_shortcode('ve_special_offer', array('Ve_Special_Offer_Shortcode', 'add_shortcode'));
            add_action( 'init', array('Ve_Special_Offer', 'add_post_type'),1000 );
        }

        public function constants() {
            if( !defined('VE_SPECIAL_OFFER_PLUGIN_DIR') ) { define('VE_SPECIAL_OFFER_PLUGIN_DIR', plugin_dir_path( __FILE__ )); }
            if( !defined('VE_SPECIAL_OFFER_PLUGIN_URL') ) { define('VE_SPECIAL_OFFER_PLUGIN_URL', plugin_dir_url( __FILE__ )); }
            if( !defined('VE_SPECIAL_OFFER_PLUGIN_FILE') ) { define('VE_SPECIAL_OFFER_PLUGIN_FILE', __FILE__ ); }
            if( !defined('VE_SPECIAL_OFFER_PLUGIN_VERSION') ) { define('VE_SPECIAL_OFFER_PLUGIN_VERSION', '0.1.0'); }
        }

        public function includes(){
            require_once( VE_SPECIAL_OFFER_PLUGIN_DIR . 'includes/admin-class.php');
            require_once( VE_SPECIAL_OFFER_PLUGIN_DIR . 'includes/front-class.php');
            require_once( VE_SPECIAL_OFFER_PLUGIN_DIR . 'includes/shortcode-class.php');
            require_once( VE_SPECIAL_OFFER_PLUGIN_DIR . 'includes/post-helper-class.php');
        }

        # Add plugin localizations
        public function load_localisation_files(){
            load_plugin_textdomain( 've_special_offer', false, dirname( plugin_basename( VE_SPECIAL_OFFER_PLUGIN_FILE ) ) . '/languages/' );
        }

        public static function get_options(){

            if( self::$options == null ) {

                $options = get_option( 've_special_offer_settings' );

                if( empty($options) ){
                    $options = Ve_Special_Offer::instance()->default_options;
                }

                self::$options = $options;

                /* set new options on updates */
                foreach( Ve_Special_Offer::instance()->default_options as $key => $value ) {

                    if( !isset( self::$options[$key] ) ) {
                        self::$options[$key] = Ve_Special_Offer::instance()->default_options[$key];
                    }

                }
            }

            return self::$options;

        }

        public static function add_post_type() {
            $params = array(
                'labels' => array(
                    'name' => __('Special Offer', 've_special_offer'),
                    'singular_name' => __('Special Offer', 've_special_offer'),
                    'add_new' => __('Add new Special Offer', 've_special_offer'),
                    'add_new_item' => __('Add new Special Offer', 've_special_offer'),
                    'edit_item' => __('Edit Special Offer', 've_special_offer'),
                    'new_item' => __('New Special Offer', 've_special_offer'),
                    'view_item' => __('View Special Offer', 've_special_offer'),
                    'view_items' => __('View Special Offers', 've_special_offer'),
                    'search_items' => __('Search Special Offers', 've_special_offer'),
                    'not_found' => __('No Special Offers found', 've_special_offer'),
                    'not_found_in_trash' => __('No Special Offers found in trash', 've_special_offer'),
                    'all_items' => __('All Special Offers', 've_special_offer'),
                ),
                'public' => true,
                'has_archive' => false,
                'hierarchical' => false,
                'rewrite' => false,
                'show_in_menu' => true,
                'show_in_nav_menus' => false,
                'menu_position' => 100,
                'show_ui' => true,
                'exclude_from_search' => true,
                'menu_icon' => 'dashicons-store',
                'supports' => array(
                    'title',
                    'editor',
                    'page-attributes',
                ),
            );

            register_post_type('special_offer', $params);
        }
    }
}

Ve_Special_Offer::instance();
jQuery(document).ready(function($){

    var dateClicked = false;

    $('.special_offer_date_selector_popup .close_popup').on('click', function(e){
        if ($('.special_offer_popup_error').is(':visible')){
            return false;
        }

        $('.special_offer_date_selector_popup').hide();
    });

    $('.special_offer_date_selector_month').on('change', function(){
        debugger;
        var value = $(this).val();

        fillDaySelector(value);
    });

    $('.special_offer_date_selector_popup form input[type="submit"]').on('click', function(e){
        e.preventDefault();

        var day = $('.special_offer_date_selector_popup [name="day"]').val();
        var month = $('.special_offer_date_selector_popup [name="month"]').val();
        var monthKey = 'month' + month;
        var selectedMonth = monthNames[monthKey];

        /*var oldValues = {
            arrivalDay: $(dateClicked).parents().eq(3).find('.special_offer_calendar_wrapper.arrival .special_offer_day').data('day'),
            arrivalMonth: $(dateClicked).parents().eq(3).find('.special_offer_calendar_wrapper.arrival .special_offer_month').data('month'),
            departureDay: $(dateClicked).parents().eq(3).find('.special_offer_calendar_wrapper.departure .special_offer_day').data('day'),
            departureMonth: $(dateClicked).parents().eq(3).find('.special_offer_calendar_wrapper.departure .special_offer_month').data('month'),
            costPerDay: $(dateClicked).parents().eq(6).find('.offer_cost').text()
        };*/

        $(dateClicked).children('.special_offer_day').text(day);
        $(dateClicked).children('.special_offer_day').data('day', day);

        $(dateClicked).children('.special_offer_month').html('<i>' + selectedMonth + '</i>');
        $(dateClicked).children('.special_offer_month').data('month', month);

        var arrivalDay = parseInt($(dateClicked).parents().eq(3).find('.special_offer_calendar_wrapper.arrival .special_offer_day').data('day'));
        var arrivalMonth = parseInt($(dateClicked).parents().eq(3).find('.special_offer_calendar_wrapper.arrival .special_offer_month').data('month'));

        var departureDay = parseInt($(dateClicked).parents().eq(3).find('.special_offer_calendar_wrapper.departure .special_offer_day').data('day'));
        var departureMonth = parseInt($(dateClicked).parents().eq(3).find('.special_offer_calendar_wrapper.departure .special_offer_month').data('month'));

        if ((arrivalMonth > departureMonth) || ((arrivalMonth == departureMonth) && arrivalDay > departureDay)){
            $('.special_offer_popup_error').show();
        } else {

            var costPerDay = parseInt($(dateClicked).parents().eq(6).find('.offer_cost').text());

            var dateData = {
                arrivalDay: arrivalDay,
                arrivalMonth: arrivalMonth,
                departureDay: departureDay,
                departureMonth: departureMonth,
                costPerDay: costPerDay,
            };

            var total = calculateTotalPrice(dateData);

            // Set total price
            $(dateClicked).parents().eq(6).find('.special_offer_total_price_value').text(total);

            $('.special_offer_popup_error').hide();
            $('.special_offer_date_selector_popup').hide();
        }
    });

    $('.special_offer_calendar_wrapper').on('click', function(e){

        var month = $(this).children('.special_offer_month').data('month');
        var day = $(this).children('.special_offer_day').data('day');
        var thisClass = '.' + $(this).attr('class');

        dateClicked = this;

        //var startDate = $(thisClass + "[data-date-start]").data('date-start');
        //var endDate = $(thisClass + "[data-date-end]").data('date-end');

        var data = {
            day: day,
            month: month,
            xValue: e.clientX,
            yValue: e.clientY,
        };

        showDateSelector(data);

        var offerCost = $(this).parents('.special_offer').find('.offer_cost').text();
        var total = $(this).parents('.special_offer').find('.special_offer_total_price_value').text();

    });

    function showDateSelector(data){

        $('.special_offer_date_selector_month option').each(function(index, element){
            if ($(element).val() == data.month){
                $(element).prop('selected', true);
            }
        });

        fillDaySelector(data.month);

        var wrapperWidth = $('.special_offers_wrapper').width();
        var wrapperHeight = $('.special_offers_wrapper').height();

        $('.special_offer_date_selector_popup').show();
        $('.special_offer_date_selector_popup').css({"top":data.yValue - wrapperHeight,"left":data.xValue - wrapperWidth})
    }

    function calculateTotalPrice(data){

        debugger;
        var totalDays = 0;

        var currentDate = new Date();
        var fullYear = currentDate.getFullYear();

        if (data.arrivalMonth < data.departureMonth){
            var arrivalMonthLength = parseInt(new Date(fullYear, data.arrivalMonth, 0).getDate());
            var restInArrivalMonth = arrivalMonthLength - data.arrivalDay + 1;
            totalDays += restInArrivalMonth;
        }
        var otherDays = 0;
        for (var month = data.arrivalMonth+1; month < data.departureMonth; month++){
            otherDays += parseInt(new Date(fullYear, month, 0).getDate());
        }

        totalDays += otherDays;

        if (data.arrivalMonth == data.departureMonth){
            totalDays = data.departureDay - data.arrivalDay + 1;
        } else {
            totalDays += data.departureDay;
        }

        var totalPrice = totalDays  * data.costPerDay;

        return totalPrice;
    }

    function fillDaySelector(month, day){

        if (typeof day == "undefined"){
            day = 0;
        }

        var dayOptions = '';

        var currentDate = new Date();
        var endDay = new Date(currentDate.getFullYear(), month, 0).getDate();

        for (var i = 1; i <= endDay; i++){
            var selectedDay = '';
            if (day == i){
                selectedDay = ' selected';
            }
            dayOptions += '<option' + selectedDay + ' value="' + i + '">' + i + '</option>';
        }
        $('.special_offer_date_selector_day').html(dayOptions);
    }
});
